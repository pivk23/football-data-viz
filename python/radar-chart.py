import matplotlib.pyplot as plt
from soccerplots.radar_chart import Radar

# parameter names
params = ['Shot Creation Actions', 'Goal Creation Actions', 'Key Passes', 'Passes Into Final 1/3', 'Passes Into the Box',
          'Successfull Dribbles', 'Carries Into Final 1/3', 'Carries Into the Box']

# range values
ranges = [(1.0, 7.79), (0.1, 1.45), (0.1, 4.4), (2, 9), (0.1, 3.87),
          (0.2, 5), (0.2, 5.37), (0.1, 3.41)]

# parameter value
values = [
    [7.46, 1.33, 4.3, 7.4, 2.4, 4, 5.37, 1.67],  # for Sergino Dest
    [3.48, 0.35, 1.0, 2.8, 1.04, 1.84, 1.34, 2.04]  # for Nelson Semedo
]

bg = "#1c1f23"
patch = "#535a63"

# instantiate object
radar = Radar(background_color="white", patch_color="#d6d6d6", fontfamily="Arial",
              label_fontsize=14, range_fontsize=10, label_color="#000000", range_color="#000000")

# plot radar -- compare
fig, ax = radar.plot_radar(ranges=ranges, params=params, values=values,
                           radar_color=[(0.854, 0.321, 0.349, 0.6),
                                        (0, 0.760, 0.160, 0.4)],
                           compare=True)

plt.show()
