import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from matplotlib.legend_handler import HandlerPatch
from mplsoccer.pitch import Pitch, VerticalPitch
from matplotlib.colors import LinearSegmentedColormap


def statsbombed(data):
    data.x *= 1.2
    data.y *= 0.8
    if 'endX' in data:
        data.endX *= 1.2
        data.endY *= 0.8


def invert_y(data):
    data.y = abs(data.y - 80)
    data.endY = abs(data.endY - 80)


def adapt_passing(data):
    statsbombed(data)
    invert_y(data)
    x = data['x'].to_numpy().flatten()
    y = data['y'].to_numpy().flatten()
    endX = data['endX'].to_numpy().flatten()
    endY = data['endY'].to_numpy().flatten()

    passes = {
        'x': x,
        'y': y,
        'endX': endX,
        'endY': endY
    }
    return passes


# setup figure
fig, (ax1, ax2) = plt.subplots(ncols=2, nrows=1, figsize=(13, 10))
fig.set_facecolor('#1c1f23')
ax1.patch.set_facecolor('#1c1f23')
ax2.patch.set_facecolor('#1c1f23')

pitch = VerticalPitch(
    figsize=(13, 8),
    orientation='vertical',
    pitch_type='statsbomb',
    pitch_color='#1c1f23',
    line_color="white",
    line_zorder=2,
    tight_layout=True,
)

pitch.draw(ax=ax1)
pitch.draw(ax=ax2)

# heatmap
touches = pd.read_csv('data\Kroos-touches.csv')
statsbombed(touches)

heatmap_cmap = LinearSegmentedColormap.from_list(
    "Flamingo - 100 colors", ['#1c1f23', '#492528', '#922f31', '#ff3e3e'], N=100
)

kde = pitch.kdeplot(
    touches.x, touches.y, ax=ax2,
    shade=True, levels=500, shade_lowest=True,
    thresh=0.1,
    cmap=heatmap_cmap
)


# tackles/dribbles/touches

r""" 
# torvaney & whoscored
dribbles = pd.read_csv(
    r"C:\Users\Korisnik\Desktop\football data-viz\data\Juan Cuadrado\cuadrado-dribbles.csv")
statsbombed(dribbles)

dribble = pitch.scatter(dribbles['x'], dribbles['y'], ax=ax,
                        color='teal', label='dribble')
 """

# plot passes

# chalkboard
kroosPassing = pd.read_csv('data\Kroos-passing.csv')
kroosPassing = adapt_passing(kroosPassing)

# plot passes
passes = pitch.scatter(
    kroosPassing["x"], kroosPassing["y"], ax=ax1, color='#2dc23e', s=8)
for i in range(len(kroosPassing["x"])):
    ax1.arrow(x=kroosPassing["y"][i], y=kroosPassing["x"][i], dx=kroosPassing["endY"]
              [i] - kroosPassing["y"][i], dy=kroosPassing["endX"][i] - kroosPassing["x"][i],
              color='#2dc23e', length_includes_head=True, head_width=1)


# legend setup
def make_legend_arrow(legend, orig_handle, xdescent, ydescent, width, height, fontsize):
    p = mpatches.FancyArrow(0, 0.5*height, width, 0,
                            length_includes_head=True, head_width=5)
    return p


arrow1 = mpatches.FancyArrow(
    0, 0, 0, 0, color='#2dc23e')

ax1.legend([arrow1], ['long passes'], handler_map={mpatches.FancyArrow: HandlerPatch(
    patch_func=make_legend_arrow)}, handlelength=4, handletextpad=1, borderpad=1.5, loc=(0.06, 0.185))


plt.tight_layout()

plt.show()
