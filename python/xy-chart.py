import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patheffects as path_effects
from adjustText import adjust_text

# data
names = ['Alaves', 'Athletic Bilbao', 'Atletico Madrid',
         'Barcelona', 'Cadiz', 'Celta', 'Eibar',
         'Elche', 'Getafe', 'Granada', 'Levante', 'Osasuna',
         'Betis', 'Real Madrid', 'Real Sociedad', 'Valladolid',
         'Huesca', 'Sevilla', 'Valencia', 'Villarreal']
xP = [26, 28, 39, 43, 25, 38, 25, 12, 26, 25,
      28, 27, 29, 41, 42, 23, 21, 38, 24, 40]
pts = [22, 25, 54, 43, 24, 29, 20, 18, 24, 30,
       27, 22, 30, 46, 35, 20, 16, 42, 24, 36]

df = pd.DataFrame({
    'Team': names,
    'xP': xP,
    'Points': pts
})

# highlight certain teams/players
highlighted = ['Atletico Madrid', 'Barcelona', 'Real Madrid',
               'Sevilla', 'Real Sociedad', 'Villarreal']

# setup figure
fig, ax = plt.subplots(figsize=(10, 7))
fig.set_facecolor('#1c1f23')
ax.patch.set_facecolor('#1c1f23')

ax.set_xlim(20, 45)
ax.set_ylim(15, 60)

ax.spines["top"].set_color("#2b2b2b")
ax.spines["bottom"].set_color("gray")
ax.spines["left"].set_color("gray")
ax.spines["right"].set_color("#2b2b2b")
ax.tick_params(axis='both', color="white", labelcolor="white", labelsize=8)
ax.set_xlabel("Expected Points", color="#ffffff", fontsize=14)
ax.set_ylabel("Points", color="#ffffff", fontsize=14)

ax.grid(b=True, alpha=0.05, axis="both")

# plot data
for _, row_val in df.iterrows():
    if row_val['Team'] in highlighted:
        alpha, s, ec = 1, 100, '#f54545'
    else:
        alpha, s, ec = 0.6, 60, 'grey'

    ax.scatter(
        row_val["xP"], row_val["Points"],
        s=s, hatch=8*"/", edgecolor=ec, fc='#1c1f23', alpha=alpha, zorder=4
    )

# plot data annotations
text_values = df.loc[
    df['Team'].isin(highlighted),
    ["xP", "Points", "Team"]
].values

texts = [
    ax.text(
        value[0], value[1], value[2],
        size=9, color='#ffffff', zorder=5,
    ) for value in text_values
]

[text.set_path_effects([path_effects.withStroke(
    linewidth=3, foreground='#1c1f23')]) for text in texts]


adjust_text(
    texts, autoalign='y',
    only_move={'points': 'y', 'text': 'xy'},
    force_objects=(0.5, 3), force_text=(0.5, 3),
    force_points=(0.5, 7)
)

# plot median lines
median_x = df["xP"].median()
median_y = df["Points"].median()

ax.plot([median_x, median_x], [15, 60],
        color='#ffffff', ls="--", lw=0.5, alpha=0.7, zorder=1)
ax.plot([20, 45], [median_y, median_y],
        color='#ffffff', ls="--", lw=0.5, alpha=0.7, zorder=1)

text_values = [
    {
        'x': median_x-0.2, 'y': 50, 's': "MEDIAN xP", "color": "#ffffff",
        "ha": "center", "va": "center", "rotation": 90, "fontsize": 8, "zorder": 2, "alpha": 0.75
    },
    {
        'x': 40, 'y': median_y-0.8, 's': "MEDIAN Points", "color": "#ffffff",
        "ha": "left", "va": "center", "fontsize": 8, "zorder": 2, "alpha": 0.75
    }
]

texts = [ax.text(**txt) for txt in text_values]

# plot linear regression
x = np.array(xP)
y = np.array(pts)
a, b = np.polyfit(x, y, 1)


def extended(ax, x, y, **args):

    xlim = ax.get_xlim()
    ylim = ax.get_ylim()

    x_ext = np.linspace(xlim[0], xlim[1], 100)
    p = np.polyfit(x, y, deg=1)
    y_ext = np.poly1d(p)(x_ext)
    ax.plot(x_ext, y_ext, **args)
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    return ax


ax = extended(ax, x, y,  color="white", lw=1, ls='--', alpha=0.75)

plt.show()
