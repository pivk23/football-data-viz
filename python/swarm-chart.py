import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.gridspec import GridSpec

bg = "#1c1f23"

# data
players = pd.read_csv('data\la-liga.csv')
players = players[players.xG > 1]
players = players[players.xA > 0.5]

goals = players[['Gls']].to_numpy().flatten()
assists = players[['Ast']].to_numpy().flatten()
xG = players[['xG']].to_numpy().flatten()
xA = players[['xA']].to_numpy().flatten()

# setup figure
fig = plt.figure(figsize=(12, 7.5))
gs = GridSpec(4, 1, figure=fig)
fig.patch.set_facecolor(bg)

#######################################################################
# ax1
ax1 = fig.add_subplot(gs[0, 0:])
ax1.set_facecolor(bg)

# ax1 plot
sns.swarmplot(x=goals, size=8, fc='#57baff',
              alpha=1, zorder=2)
sns.swarmplot(x=[11], size=12, color='#ff5f5f',
              edgecolor='black', linewidth=1, zorder=3)
ax1.text(11, -0.18, "Marcos Llorente", color="white",
         fontsize='11', horizontalalignment='center')

# ax1 customize
ax1.spines["top"].set_visible(False)
ax1.spines["bottom"].set_visible(False)
ax1.spines["left"].set_visible(False)
ax1.spines["right"].set_visible(False)
ax1.grid(axis='x', color="#7a7a7a", linewidth=0.5, zorder=1)
ax1.get_yaxis().set_visible(False)
ax1.tick_params(axis='both', color="white", labelcolor="white")

#######################################################################
# ax2
ax2 = fig.add_subplot(gs[1, 0:])
ax2.set_facecolor(bg)

# ax2 plot
sns.swarmplot(x=xG, size=8, fc='#57baff',
              alpha=1, zorder=2)
sns.swarmplot(x=[3.85], size=12, color='#ff5f5f',
              edgecolor='black', linewidth=1, zorder=2)
ax2.text(3.85, -0.25, "Marcos Llorente", color="white",
         fontsize='11', horizontalalignment='center')

# ax2 customize
ax2.spines["top"].set_visible(False)
ax2.spines["bottom"].set_visible(False)
ax2.spines["left"].set_visible(False)
ax2.spines["right"].set_visible(False)
ax2.grid(axis='x', color="#7a7a7a", linewidth=0.5, zorder=1)
ax2.get_yaxis().set_visible(False)
ax2.tick_params(axis='both', color="white", labelcolor="white")

#######################################################################
# ax3
ax3 = fig.add_subplot(gs[2, 0:])
ax3.set_facecolor(bg)

# ax3 plot
sns.swarmplot(x=assists, size=8, fc='#57baff',
              alpha=1, zorder=2)
sns.swarmplot(x=[8], size=12, color='#ff5f5f',
              edgecolor='black', linewidth=1, zorder=2)
ax3.text(8, -0.25, "Marcos Llorente", color="white",
         fontsize='11', horizontalalignment='center')

# ax3 customize
ax3.spines["top"].set_visible(False)
ax3.spines["bottom"].set_visible(False)
ax3.spines["left"].set_visible(False)
ax3.spines["right"].set_visible(False)
ax3.grid(axis='x', color="#7a7a7a", linewidth=0.5, zorder=1)
ax3.get_yaxis().set_visible(False)
ax3.tick_params(axis='both', color="white", labelcolor="white")

#######################################################################
# ax4
ax4 = fig.add_subplot(gs[3, 0:])
ax4.set_facecolor(bg)

# ax4 plot
sns.swarmplot(x=xA, size=8, fc='#57baff',
              alpha=1, zorder=2)
sns.swarmplot(x=[2.8], size=12, color='#ff5f5f',
              edgecolor='black', linewidth=1, zorder=2)
ax4.text(2.8, -0.25, "Marcos Llorente", color="white",
         fontsize='11', horizontalalignment='center')

# ax4 customize
ax4.spines["top"].set_visible(False)
ax4.spines["bottom"].set_visible(False)
ax4.spines["left"].set_visible(False)
ax4.spines["right"].set_visible(False)
ax4.grid(axis='x', color="#7a7a7a", linewidth=0.5, zorder=1)
ax4.get_yaxis().set_visible(False)
ax4.tick_params(axis='both', color="white", labelcolor="white")

# padding
plt.subplots_adjust(left=None, bottom=None, right=None,
                    top=None, wspace=None, hspace=0.8)

plt.show()
