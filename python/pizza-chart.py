import matplotlib.pyplot as plt
from mplsoccer import PyPizza

# parameter list
params = [
    "Non-Penalty\nGoals", "Non-Penalty xG", "Goals/Shot", "xA", "Key Passes", "Passes into\nPenalty Area", "Carries into\nPenalty Area",
    "Passes into\nFinal 1/3", "Progressive\nPasses", "Carries into\nFinal 1/3", "Progressive\nCarries", "Dribbles\nCompleted",
    "Pressures", "Successful\nPressures %", "Dribbled\npast", "Tackles +\nInterceptions", "% Arials Won"
]

# value list
# (has to be same length of params)
values = [
    68, 79, 99, 77, 70, 80, 83,
    90, 78, 96, 98, 79,
    5, 76, 95, 6, 94
]

# slice & text color (multiplied by number of slices in one section)
slice_colors = ["#59b53f"] * 5 + ["#cfb523"] * 7 + ["#d43f3f"] * 5
text_colors = ["#000000"] * 7 + ["#000000"] * 5 + ["#000000"] * 5

# instantiate PyPizza class
baker = PyPizza(
    params=params,
    background_color="#1c1f23",
    straight_line_color="#ffffff", straight_line_lw=2, straight_line_ls='-',
    inner_circle_size=40,
    last_circle_color="#ffffff", last_circle_lw=2, last_circle_ls='-',
    other_circle_color="#ffffff", other_circle_lw=0, other_circle_ls="--",
)

# plot pizza
fig, ax = baker.make_pizza(

    # main arguments
    figsize=(13.5, 8),
    param_location=114,
    values=values,
    value_colors=text_colors,
    slice_colors=slice_colors,
    value_bck_colors=slice_colors,
    color_blank_space="same",
    blank_alpha=0.1,

    # tweak parameters apperance
    kwargs_params=dict(
        color="#FFFFFF",
        fontsize=10,
        va="center"
    ),

    # tweak slices apperance
    kwargs_slices=dict(
        edgecolor="#F2F2F2",
        zorder=2,
        linewidth=1
    ),

    # tweak values apperance
    kwargs_values=dict(
        color="#FFFFFF",
        fontsize=10,
        va='center',
        zorder=3,
        bbox=dict(
            edgecolor="#000000",
            facecolor="#ffffff",
            boxstyle="round, pad=0.2",
            lw=0.75
        )
    )
)

plt.show()
