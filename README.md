<div align="center">

## FOOTBALL DATA VISUALIZATIONS

#### Pitch-template

<p float="left">
    <img width="400" src="https://gitlab.com/pivk23/football-data-viz/-/raw/master/examples/pitch-template.jpg">
    <img  width="400" src="https://gitlab.com/pivk23/football-data-viz/-/raw/master/examples/pitch-template-white.jpg">
</p>

#### XY-chart

<p float="left">
    <img width="400" src="https://gitlab.com/pivk23/football-data-viz/-/raw/master/examples/xy-chart.jpg">
    <img  width="400" src="https://gitlab.com/pivk23/football-data-viz/-/raw/master/examples/xy-chart-white.jpg">
</p>

#### Pizza-chart

<p float="left">
    <img width="400" src="https://gitlab.com/pivk23/football-data-viz/-/raw/master/examples/pizza-chart.jpg">
    <img  width="400" src="https://gitlab.com/pivk23/football-data-viz/-/raw/master/examples/pizza-chart-white.jpg">
</p>

#### Radar-chart

<p float="left">
    <img width="400" src="https://gitlab.com/pivk23/football-data-viz/-/raw/master/examples/radar-chart.jpg">
    <img  width="400" src="https://gitlab.com/pivk23/football-data-viz/-/raw/master/examples/radar-chart-white.jpg">
</p>

#### Swarm-chart

<p float="left">
    <img width="400" src="https://gitlab.com/pivk23/football-data-viz/-/raw/master/examples/swarm-chart.jpg">
    <img  width="400" src="https://gitlab.com/pivk23/football-data-viz/-/raw/master/examples/swarm-chart-white.jpg">
</p>

<div>
